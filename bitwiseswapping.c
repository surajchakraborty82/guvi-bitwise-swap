#include <stdio.h>
#include <string.h>
int swap(int*, int *);
int main()
{
    int num1, num2;
    scanf("%d %d", &num1, &num2);
    swap(&num1, &num2);  
    printf("%d %d", num1, num2);
    return 0;
}
int swap(int *x, int *y)
{
    *x = *x ^ *y;
    *y = *x ^ *y;
    *x = *x ^ *y;
}
